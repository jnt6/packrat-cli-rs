//use serde_json::{Result, Value};
use chrono;
use serde::{Deserialize, Serialize};
use serde_json::{json, Error, Value};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
enum VolCommand {
    Ls,
    Find {
        volume_id: u64,
    },
    Create {
        #[structopt(subcommand)]
        share_type: ShareType,
    },
}

#[derive(StructOpt, Debug)]
struct CreateOpts {
    max_gb: u32,
    name: String,
    location_id: u64,
    fund_code: String,
}

#[derive(StructOpt, Debug)]
enum ShareType {
    #[structopt(name = "smb")]
    Smb(CreateOpts),
    #[structopt(name = "nfs")]
    Nfs(CreateOpts),
    #[structopt(name = "s3")]
    S3(CreateOpts),
}

#[derive(StructOpt, Debug)]
enum TokenCommand {
    Ls,
    Find,
}

#[derive(StructOpt, Debug)]
enum LocationCommand {
    Ls,
    Find { location_id: u64 },
}

#[derive(StructOpt, Debug)]
struct Cli {
    #[structopt(subcommand)]
    command: InnerCli,
    #[structopt(name = "config", long, short, default_value = "./conf_file.json")]
    config: PathBuf,
}

#[derive(StructOpt, Debug)]
enum InnerCli {
    Volume {
        #[structopt(subcommand)] // Note that we mark a field as a subcommand
        subcommand: VolCommand,
    },
    Token {
        #[structopt(subcommand)] // Note that we mark a field as a subcommand
        subcommand: TokenCommand,
    },
    Location {
        #[structopt(subcommand)] // Note that we mark a field as a subcommand
        subcommand: LocationCommand,
    },
}

// This object consumes the initial configuration and provides access
// to the persisted reqwest client
struct Connection {
    client: reqwest::blocking::Client,
    conf: Conf,
    timestamp: Option<chrono::DateTime<chrono::Utc>>,
    conf_fname: PathBuf,
}

#[derive(Debug, Deserialize, Serialize)]
struct Location {
    id: i64,
    name: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct Volume {
    id: i64,
    #[serde(rename = "type")]
    tipe: String,
    name: String,
    fund_code: Option<String>,
    grouper_group_id: Option<i64>,
    location_id: i64,
    active: bool,
    api_identifier: String,
    pricing: Option<String>,
    max_gb: Option<i64>,
    used_gb: Option<i64>,
    decommissioned_at: Option<chrono::DateTime<chrono::Utc>>,
    decomissioned_size: Option<i64>,
    backups_on_site_retention: i64,
    backups_off_site_retention: i64,
    created_at: chrono::DateTime<chrono::Utc>,
    updated_at: chrono::DateTime<chrono::Utc>,
    url: String,
    user: Option<String>,
    grouper_group: Option<String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct Conf {
    token: String,
    jwt_token: Option<String>,
    packrat_url: String,
    client_id: String,
    idms_url: String,
    endpoint: String,
    ts_text: String,
}

impl Connection {
    pub fn post(&self, url: String, json: serde_json::Value) -> String {
        self.client
            .post(format!("{}/{}", self.conf.packrat_url, &url))
            .json(&json)
            .header("Authorization", self.conf.auth_header())
            .send()
            .unwrap()
            .text()
            .unwrap()
    }
    pub fn get_text(&self, url: String) -> String {
        self.client
            .get(format!("{}/{}", self.conf.packrat_url, &url))
            .header("Authorization", self.conf.auth_header())
            .send()
            .unwrap()
            .text()
            .unwrap()
    }

    pub fn new(conf: Conf, conf_fname: &PathBuf) -> Self {
        Connection {
            conf,
            timestamp: None,
            client: reqwest::blocking::Client::new(),
            conf_fname: conf_fname.to_path_buf(),
        }
    }
    pub fn update_token(&mut self) {
        let now = chrono::Utc::now();
        let mut needs_update = true;
        // Does our in-memory timestamp look good? If so, do not update
        match self.timestamp {
            Some(ts) => {
                if &now.signed_duration_since(ts).num_seconds() < &600 {
                    needs_update = false;
                }
            }
            None => {}
        }
        if !needs_update {
            return;
        }
        // If we fell through, we use the cached timestamp / token
        let ts = self.conf.timestamp();
        dbg!("TS is {:?} ", ts);
        let token_age = now.signed_duration_since(ts).num_seconds();
        dbg!("{:?}", token_age);
        let mut needs_write = false;
        if &token_age > &600 || None == self.conf.jwt_token {
            dbg!("No token found, getting a new one");
            self.conf.jwt_token = Some(self.fetch_short_lived_token());
            self.conf.ts_text = serde_json::to_string(&now).unwrap().replace("\"", "");
            self.timestamp = Some(now);
            needs_write = true;
        } else {
            dbg!("Using a cached token from the conf file");
        }
        if needs_write {
            std::fs::write(
                &self.conf_fname,
                serde_json::to_string_pretty(&self.conf).unwrap(),
            )
            .expect("Could not write conf file");
        }
    }

    fn fetch_short_lived_token(&mut self) -> String {
        let token = self.get_token();
        let json: Value = token.json().unwrap();
        let error = &json.get("error").unwrap();
        match error {
            Value::Bool(field) => match field {
                false => {}
                true => {
                    panic!("error getting token!");
                }
            },
            _ => {
                panic!("error getting token!");
            }
        }
        let result = &json.get("mapQueryResult").unwrap();
        match result {
            Value::Object(f) => {
                // let attrs = &f["attributes"];
                let token = &f["attributes"]["accessToken"];
                return token.to_string().replace("\"", "");
            }
            _ => {
                panic!("Got an unexpected JSON response");
            }
        }
    }

    pub fn get_token(&self) -> reqwest::blocking::Response {
        let value = serde_json::json!(
        {
            "longLivedToken": &self.conf.token,
            "clientId": &self.conf.client_id,
            "endpoints": [&self.conf.endpoint],
        });
        let res = self
            .client
            .post(&self.conf.idms_url)
            .json(&value)
            .send()
            .unwrap();
        return res;
    }
}

impl Conf {
    pub fn new() -> Self {
        Conf {
            token: "YOURTOKENHERE".to_string(),
            endpoint: "endpoint:/api/v2".to_string(),
            jwt_token: None,
            ts_text: "2020-03-28 17:57:04.222452521 UTC".to_string(),
            packrat_url: "https://packrat.oit.duke.edu".to_string(),
            client_id: "packrat-production".to_string(),
            idms_url: "https://idms-web-ws.oit.duke.edu/idm-ws/clientSecret/createClientToken"
                .to_string(),
        }
    }

    pub fn auth_header(&self) -> String {
        format!("Bearer {}", self.jwt_token.as_ref().unwrap())
    }

    pub fn timestamp(&self) -> chrono::DateTime<chrono::Utc> {
        // Use the timestamp from the conf file, if it is there and parseable
        // otherwise, just set it to one day ago
        let ts_str = format!("\"{}\"", self.ts_text);
        let sj = serde_json::from_str(&ts_str);
        match sj {
            Ok(date) => date,
            Err(_e) => chrono::Utc::now() - chrono::Duration::days(1),
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();
    let conf_fname = &args.config;
    let conf = read_conf(&conf_fname);
    let mut client = Connection::new(conf, &conf_fname);
    client.update_token();
    dbg!("{:?}", &args);
    // let action = args.verb;
    println!("{:?}", args);
    match &args.command {
        InnerCli::Volume { subcommand } => process_volumes_command(subcommand, &client),
        InnerCli::Token { subcommand } => process_token_command(subcommand, &client),
        InnerCli::Location { subcommand } => process_location_command(subcommand, &client),
    }
    Ok(())
}

fn read_conf(fname: &PathBuf) -> Conf {
    let conf: Conf;
    let conf_json = std::fs::read_to_string(fname);
    match conf_json {
        Ok(data) => {
            dbg!("Found a conf file!");
            conf = serde_json::from_str(&data).unwrap();
        }
        Err(_) => {
            dbg!("Found no conf file!");
            conf = Conf::new()
        }
    }
    return conf;
}

fn help() {
    eprintln!("Invalid input, please see the non-existant documentation!");
    std::process::exit(1);
}

fn process_location_command(command: &LocationCommand, client: &Connection) {
    match command {
        LocationCommand::Ls => {
            let serde = fetch_locations(&client);
            dbg!("Serde {:?}", serde);
            dbg!("Got Ls");
        }
        LocationCommand::Find { location_id } => {
            let serde = fetch_location(&client, location_id);
            dbg!("Serde {:?}", serde);
            dbg!("Got Find");
        }
    }
}

fn process_volumes_command(command: &VolCommand, client: &Connection) {
    match command {
        VolCommand::Ls => {
            let serde = fetch_volumes(&client);
            dbg!("Serde {:?}", serde);
            dbg!("Got Ls");
        }
        VolCommand::Find { volume_id } => {
            let serde = fetch_volume(&client, volume_id);
            dbg!("Serde {:?}", serde);
            dbg!("Got Find");
        }
        VolCommand::Create { share_type } => {
            create_volume(&client, share_type);
        }
    }
}

fn process_token_command(command: &TokenCommand, client: &Connection) {
    match command {
        TokenCommand::Ls => {
            println!("{}", client.get_token().text().unwrap());
        }
        _ => help(),
    }
}

fn fetch_location(client: &Connection, id: &u64) -> Location {
    let url = format!("api/v2/locations/{}", id);
    let result = client.get_text(url.to_string());
    let location: Location = serde_json::from_str(&result).unwrap();
    location
}

fn fetch_locations(client: &Connection) -> Vec<Location> {
    let content_str = client.get_text("api/v2/locations".to_string());
    let locations: Vec<Location> = serde_json::from_str(&content_str).unwrap();
    // Would get a wrapped serde json Object
    // let json = client.get_json("api/v2/locations".to_string());
    // match &json {
    //     Ok(t) => { dbg!("{:?}", t ); },
    //     Err(e) => { dbg!("{:?}", e ); }
    // }
    dbg!("{}", &locations);
    locations
}

fn fetch_volume(client: &Connection, id: &u64) -> Volume {
    let url = format!("api/v2/volumes/{}", id);
    let result = client.get_text(url.to_string());
    let volume: Volume = serde_json::from_str(&result).unwrap();
    volume
}

fn fetch_volumes(client: &Connection) -> Vec<Volume> {
    let content_str = client.get_text("api/v2/volumes".to_string());
    let volumes: Vec<Volume> = serde_json::from_str(&content_str).unwrap();
    dbg!("{}", &volumes);
    volumes
}

fn create_volume(client: &Connection, share_type: &ShareType) {
    let mut share_nfs = false;
    let mut share_smb = false;
    let mut share_s3 = false;
    dbg!("{}", share_type);
    let final_opts: &CreateOpts;

    match share_type {
        ShareType::S3(opts) => {
            share_s3 = true;
            final_opts = opts
        }
        ShareType::Smb(opts) => {
            share_smb = true;
            final_opts = opts
        }
        ShareType::Nfs(opts) => {
            share_nfs = true;
            final_opts = opts
        }
    }
    let request = json!({
        "name": final_opts.name,
        "location_id": final_opts.location_id,
        "fund_code": final_opts.fund_code,
        "requested_state": {
            "max_gb": final_opts.max_gb,
            "share_nfs": share_nfs,
            "share_s3": share_s3,
            "share_smb": share_smb,
        }
    });
    let text = client.post("api/v2/volumes".to_string(), request);
    dbg!("{}", text);
}

#[test]
fn check_list_output() {}
